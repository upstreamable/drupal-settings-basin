<?php

if (getenv('SMTP_GLOBAL_SENDER_IS_USERNAME') && getenv('SMTP_USERNAME')) {
  $config['system.site']['mail'] = getenv('SMTP_USERNAME');
}
